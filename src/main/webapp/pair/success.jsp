<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="bean.*"%>
<%@ page import="dao.UserDao"%>
<!DOCTYPE html >
<html>
<link rel="stylesheet"
	href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
<script
	src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>


	<%
		UserDao ud = new UserDao();
		List tasklist = ud.findAll();
	%>
	<div class="table-responsive">
		<div></div>
		<table class="table table-striped">
			<caption><%=session.getAttribute("username")%>登陆成功!
				<button class="btn btn-primary btn-sm" data-toggle="modal"
					data-target="#myModal1" style="margin-left: 90%">添加任务</button>
			</caption>
			<thead>
				<tr>
					<th>序号</th>
					<th>执行者</th>
					<th>详细描述</th>
					<th>开始时间</th>
					<th>结束时间</th>
					<th>状态</th>

					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<%
					for (int i = 0; i < tasklist.size(); i++) {
						Task task = (Task) tasklist.get(i);
				%>
				<tr id=<%=task.getId()+"tr" %>>
					<th><%=task.getId()%></th>
					<th><lable id="<%=task.getId() + "o11"%>"><%=task.getName()%></lable>
						<input type="text" class="form-control" name="o1"
						id=<%=task.getId() + "o1"%> value=<%=task.getName()%>></th>

					<th><lable id="<%=task.getId() + "content1"%>"><%=task.getcontent()%></lable>
						<textarea class="form-control" rows="5" name="content"
							id="<%=task.getId() + "content"%>" value=<%=task.getcontent()%>></textarea>
					</th>

					<th><lable id="<%=task.getId() + "start1"%>"><%=task.getBtime()%></lable>
						<input type="date" class="form-control" name="start"
						id="<%=task.getId() + "start"%>" value="<%=task.getBtime()%>">
					</th>

					<th><lable id="<%=task.getId() + "end1"%>"><%=task.getEtime()%></lable>
						<input type="date" class="form-control" name="end"
						id="<%=task.getId() + "end"%>" value="<%=task.getEtime()%>">
					</th>

					<th><lable id="<%=task.getId() + "st1"%>"><%=task.getState()%></lable>
						<select class="form-control" value="<%=task.getState()%>" name="st" id=<%=task.getId() + "st"%>>
							<option value="未开始" >未开始</option>
							<option value="开始" >开始</option>
							<option value="结束">结束</option>
					</select></th>
					<th>
						<button type="button" class="btn btn-primary"
							id="<%=task.getId() + "confirm1"%>"
							onclick="update(<%=task.getId()%>)">修改</button>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
						<button type="button" class="btn btn-danger"
							id="<%=task.getId() + "cancel1"%>"
							onclick="deleteTR(<%=task.getId()%>)">删除</button>
						<button type="button" class="btn btn-primary" name="confirm"
							id="<%=task.getId() + "confirm"%>"
							onclick="update2(<%=task.getId()%>)">确认修改</button>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
						<button type="button" class="btn btn-danger" name="cancel"
							id="<%=task.getId() + "cancel"%>"
							onclick="cancel(<%=task.getId()%>)">取消</button>


					</th>
				</tr>
				<%
					}
				%>
			</tbody>
		</table>
	</div>
	<!--js  -->
	<script type="text/javascript">
	$(document).ready(function(){
		$("select[name='st']").hide();
		$("input[name='o1']").hide();
		$("button[name='confirm']").hide();
		$("button[name='cancel']").hide();
		$("textarea[name='content']").hide();
		$("input[name='start']").hide();
		$("input[name='end']").hide();
	
	});
	
	function deleteTR(id){
		$.get("/task/DeleteTaskServlet?id="+id, function(data){
			
		       if("Y"==data){
		    	   $("#"+id+"tr").hide();
		       }
		})
	}
	function update(id){
		$("#"+id+"st1").hide();
		$("#"+id+"st").show();
		$("#"+id+"o11").hide();
		$("#"+id+"o1").show();
		$("#"+id+"content1").hide();
		$("#"+id+"content").show();
		$("#"+id+"confirm1").hide();
		$("#"+id+"confirm").show();
		$("#"+id+"cancel1").hide();
		$("#"+id+"cancel").show();
		$("#"+id+"start1").hide();
		$("#"+id+"start").show();
		$("#"+id+"end1").hide();
		$("#"+id+"end").show();
	}
	function update2(id){
		$.get("/task/UpdateTaskServlet?owner="+$("#"+id+"o1").val()
				+"&status="+$("#"+id+"st").val()+"&content="+$("#"+id+"content").val()+"&id="+id
				+"&start="+$("#"+id+"start").val()+"&end="+$("#"+id+"end").val(), function(data){
			
		       if("Y"==data){
		    	   $("#"+id+"o11").text($("#"+id+"o1").val());
		    	   $("#"+id+"st1").text($("#"+id+"st").val());
		    	   $("#"+id+"content1").text($("#"+id+"content").val());
		    	   $("#"+id+"start1").text($("#"+id+"start").val());
		    	   $("#"+id+"end1").text($("#"+id+"end").val());
		       }
		})
		$("#"+id+"st").hide();
		$("#"+id+"st1").show();
		$("#"+id+"o1").hide();
		$("#"+id+"o11").show();
		$("#"+id+"content").hide();
		$("#"+id+"content1").show();
		$("#"+id+"confirm").hide();
		$("#"+id+"confirm1").show();
		$("#"+id+"cancel").hide();
		$("#"+id+"cancel1").show();
		$("#"+id+"start").hide();
		$("#"+id+"start1").show();
		$("#"+id+"end").hide();
		$("#"+id+"end1").show();
		
	}
	
	function cancel(id){
		$("#"+id+"st").hide();
		$("#"+id+"st1").show();
		$("#"+id+"o1").hide();
		$("#"+id+"o11").show();
		$("#"+id+"content").hide();
		$("#"+id+"content1").show();
		$("#"+id+"confirm").hide();
		$("#"+id+"confirm1").show();
		$("#"+id+"cancel").hide();
		$("#"+id+"cancel1").show();
		$("#"+id+"start").hide();
		$("#"+id+"start1").show();
		$("#"+id+"end").hide();
		$("#"+id+"end1").show();
	}
	</script>
	<!-- 添加模态框 -->
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">添加任务</h4>
				</div>
				<div class="modal-body">

					<form name="myForm" action="/task/AddTaskServlet"
						class="form-horizontal" role="form" method="post">

						<div class="form-group">
							<label for="username" class="col-sm-3 control-label">执行者</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="username"
									placeholder="请输入执行者">
							</div>
							<div class="col-sm-3"></div>
						</div>

						<div class="form-group">
							<label for="firstname" class="col-sm-3 control-label">内容</label>
							<div class="col-sm-9">
								<textarea class="form-control" rows="6" name="content"></textarea>
							</div>
							<div class="col-sm-1"></div>
						</div>


						<div class="form-group">
							<label for="username" class="col-sm-3 control-label">开始时间</label>
							<div class="col-sm-6">
								<input type="date" class="form-control" name="start"
									placeholder="请输入开始时间">
							</div>
							<div class="col-sm-3"></div>
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-3 control-label">结束时间</label>
							<div class="col-sm-6">
								<input type="date" class="form-control" name="end"
									placeholder="请输入开始时间">
							</div>
							<div class="col-sm-3"></div>
						</div>

						<div class="form-group">
							<label for="username" class="col-sm-3 control-label">状态</label>
							<div class="col-sm-6">
								<select class="form-control" name="state">
									<option value="未开始">未开始</option>
									<option value="开始">开始</option>
									<option value="结束">结束</option>
								</select>
							</div>
							<div class="col-sm-3"></div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
							<button type="submit" class="btn btn-primary">提交更改</button>
						</div>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	
</body>
</html>