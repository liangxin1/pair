<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
<script
	src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

	<form action="/task/Login" class="form-horizontal" role="form"
		method="post">
		<div class="row">
			<div class="col-sm-5"></div>
			<div class="col-sm-2">
				<h2>登录</h2>
			</div>
			<div class="col-sm-5"></div>
		</div>

		<div class="form-group">
			<label for="username" class="col-sm-3 control-label">名字</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="username"
					placeholder="请输入名字">
			</div>
			<div class="col-sm-3"></div>
		</div>

		<div class="form-group">
			<label for="userpwd" class="col-sm-3 control-label">密码</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="userpwd"
					placeholder="请输入密码">
			</div>
			<div class="col-sm-3"></div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-9">
				<button type="submit" class="btn btn-primary">登录</button>
				&nbsp&nbsp<a href="register.jsp" class="btn btn-primary">注册</a>
				&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
				<label> <input type="radio" name="optionsRadios"  value="admin"
					checked> admin
				</label> <label> <input type="radio" name="optionsRadios"
					 value="user">user
				</label>
			</div>
		</div>
	</form>
</body>
</html>