<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<body>
	<form name="myForm" action="/loginSystem/Register"
		class="form-horizontal" role="form" method="post"
		onsubmit="return validateForm()" method="post">
		<div class="row">
			<div class="col-sm-5"></div>
			<div class="col-sm-2">
				<h2>注册</h2>
			</div>
			<div class="col-sm-5"></div>
		</div>
		<div class="form-group">
			<label for="name" class="col-sm-3 control-label">执行者</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="username"
					placeholder="请输入执行者">
			</div>
			<div class="col-sm-3"></div>
		</div>
		<div class="form-group">
			<label for="content" class="col-sm-3 control-label">任务内容</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="content"
					placeholder="请输入任务内容">
			</div>
			<div class="col-sm-3"></div>
		</div>
		<div class="form-group">
			<label for="userpwd1" class="col-sm-3 control-label">再次输入密码</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="userpwd1"
					placeholder="请再次输入密码">
			</div>
			<div class="col-sm-3"></div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-9">
				<button type="submit" class="btn btn-default">注册</button>
			</div>
		</div>
	</form>
</body>
</body>
</html>