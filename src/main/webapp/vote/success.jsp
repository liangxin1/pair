<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="zz.java8.vote.dao.*"%>
<%@ page import="java.util.*"%>
<%@ page import="zz.java8.vote.bean.*"%>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
<script
	src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<%
		VotersDao vd = new VotersDao();
		List voterlist = vd.findAll();
	%>
	<table class="table table-hover">
		<caption><%=session.getAttribute("username")%>登陆成功!
		</caption>
		<thead>
			<tr>
				<th>id</th>
				<th>大师兄候选人</th>
				<th>得票数</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<%
				for (int i = 0; i < voterlist.size(); i++) {
					Voters voter = (Voters) voterlist.get(i);
			%>
			<tr>
				<td><%=voter.getId()%></td>
				<td><label id="<%=voter.getId() + "name1"%>" ><%=voter.getName()%></label></td>
				<td>
				<label id="<%=voter.getId() + "num1"%>" ><%=voter.getVoteNum()%></label>
				<input type="number" class="form-control" name="num"
						id="<%=voter.getId() + "num"%>" value=<%=voter.getVoteNum()+1%>>
				
				</td>
				<td>
				
				<button type="button" name="ban1" class="btn btn-primary" id="<%=voter.getId() + "ban1"%>" onclick="vote(<%=voter.getId()%>)">
				投他一票</button>
				
				<button type="button" class="btn btn-default btn-sm" disabled="disabled"
				name="ban"	id="<%=voter.getId() + "ban"%>">还想投几票？？？</button>
				</td>
			</tr>
			<%
				}
			%>
		</tbody>
	</table>
	
	<script type="text/javascript">
	
	$(document).ready(function(){
		$("button[name='ban']").hide();
		$("input[name='num']").hide();

	});
		function vote(id){
			$.get("/task/UpdateVoteServlet?id="+id, function(data){
				
			       if("Y"==data){
			    	   $("#"+id+"ban1").hide();
			    	   $("button[name='ban']").show();
			    	   $("button[name='ban1']").hide();
			 		  $("#"+id+"num1").text($("#"+id+"num").val());
			       }
			})
		}

		
   	</script>
</body>
</html>