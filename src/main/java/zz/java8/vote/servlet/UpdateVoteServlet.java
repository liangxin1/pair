package zz.java8.vote.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Task;
import dao.TaskDao;
import zz.java8.vote.bean.Voters;
import zz.java8.vote.dao.VotersDao;

/**
 * Servlet implementation class VoteServlet
 */
public class UpdateVoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateVoteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id=(String) request.getParameter("id");
		
		String rs="N";
		VotersDao vdao=new VotersDao();
		int aid=Integer.parseInt(id);
		Voters v=vdao.findVoter(aid);

		if(vdao.updateVote(v)){
			rs="Y";
		}
		response.getWriter().append(rs);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
