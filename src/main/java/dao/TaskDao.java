package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import bean.Task;

public class TaskDao {
	public boolean updateTask(Task t) {
		boolean f = false;
		Connection con = JdbcUtil.getConnection();
		try {
			String sql = "update task set name=?,state=?,content=?,btime=?,etime=? where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, t.getName());
			ps.setString(2, t.getState());
			ps.setString(3, t.getcontent());
			ps.setDate(4, t.getBtime());
			ps.setDate(5, t.getEtime());
			ps.setInt(6, t.getId());
			ps.execute();
			f = true;
			JdbcUtil.closeCon(null, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return f;
	}
	public boolean deleteTask(int id) {
		boolean f = false;
		Connection con = JdbcUtil.getConnection();
		try {
			String sql = "delete from task where id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ps.execute();
			f = true;
			JdbcUtil.closeCon(null, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return f;
	}
}
