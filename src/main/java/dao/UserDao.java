package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dao.JdbcUtil;
import bean.Task;
import bean.User;

public class UserDao {
	JdbcUtil JdbcUtil = new JdbcUtil();

	public User findUser(User u) {
		User user = null;
		Connection con = JdbcUtil.getConnection();
		try {
			
			String sql = "select * from user where(uname=? and upwd=?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, u.getUsername());
			ps.setString(2, u.getUserpwd());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				user = new User();
				user.setUsername(rs.getString("uname"));
				user.setUserpwd(rs.getString("upwd"));
			}
			JdbcUtil.closeCon(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return user;
	}
	
	public User findAdmin(User u) {
		User user = null;
		Connection con = JdbcUtil.getConnection();
		try {
			
			String sql = "select * from admin where(name=? and password=?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, u.getUsername());
			ps.setString(2, u.getUserpwd());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				user = new User();
				user.setUsername(rs.getString("name"));
				user.setUserpwd(rs.getString("password"));
			}
			JdbcUtil.closeCon(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return user;
	}

	public boolean addTask(Task t) {
		boolean f = false;
		Connection con = JdbcUtil.getConnection();
		try {
			String sql = "insert into task (name,content,btime,etime,state) values (?,?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, t.getName());
			ps.setString(2, t.getcontent());
			ps.setDate(3, t.getBtime());
			ps.setDate(4, t.getEtime());
			ps.setString(5, t.getState());
			ps.execute();
			f = true;
			JdbcUtil.closeCon(null, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return f;
	}

	public List findT(String name) {
		List ls = new ArrayList();
		Task task = null;
		Connection con = JdbcUtil.getConnection();
		try {
			String sql = "select * from task where name=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				task = new Task();
				task.setName(rs.getString("id"));
				task.setName(rs.getString("name"));
				task.setcontent(rs.getString("content"));
				task.setBtime(rs.getDate("btime"));
				task.setEtime(rs.getDate("etime"));
				task.setState(rs.getString("state"));
				System.out.println(1);
				ls.add(task);
			}
			JdbcUtil.closeCon(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ls;
	}
	
	
	

	public List findAll() {
		List ls = new ArrayList();
		Task task = null;
		Connection con = JdbcUtil.getConnection();
		try {
			String sql = "select * from task";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery(sql);
			while (rs.next()) {
				task = new Task();
				task.setId(rs.getInt("id"));
				task.setName(rs.getString("name"));
				task.setcontent(rs.getString("content"));
				task.setBtime(rs.getDate("btime"));
				task.setEtime(rs.getDate("etime"));
				task.setState(rs.getString("state"));
				ls.add(task);
			}
			JdbcUtil.closeCon(rs, ps, con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ls;
	}

}
