package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Task;
import dao.JdbcUtil;
import dao.UserDao;

/**
 * Servlet implementation class AddTaskServlet
 */
public class AddTaskServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddTaskServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stubue
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String name = (String) request.getParameter("username");
		String content = (String) request.getParameter("content");
		String start = (String) request.getParameter("start");
		String end = (String) request.getParameter("end");
		String state = (String) request.getParameter("state");
		Connection conn;
		Task t = new Task();
		t.setName(name);
		t.setcontent(content);
		t.setState(state);
		SimpleDateFormat sd = new SimpleDateFormat("yy-MM-dd");
		Timestamp ts;
		try {
			ts = new Timestamp(sd.parse(start).getTime());
			t.setBtime(new java.sql.Date(ts.getTime()));
			t.setEtime(new java.sql.Date(sd.parse(end).getTime()));
			UserDao ud = new UserDao();
			conn = JdbcUtil.getConnection();
			if (ud.addTask(t)) {
				
				request.getRequestDispatcher("pair/success.jsp").forward(request, response);
				// dis = request.getRequestDispatcher("pair/login.jsp");
				// dis.forward(request, response);

			} else {

				request.getRequestDispatcher("pair/fail.jsp").forward(request, response);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
