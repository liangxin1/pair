package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.Task;
import bean.User;
import dao.UserDao;

/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");	
		String a=(String) request.getParameter("optionsRadios");
		HttpSession session=request.getSession();
		if(a.equals("user")) {
			String name = request.getParameter("username");
			String pwd = request.getParameter("userpwd");
			User u=new User();
			User u2=new User();
			u.setUsername(name);
			u.setUserpwd(pwd);
			UserDao ud=new UserDao();
			u2=ud.findUser(u);
			List<Task> t=ud.findT(u.getUsername());
			if(u2!=null) {
				request.setAttribute("username", name);
				request.setAttribute("list", t);
				request.getRequestDispatcher("pair/success.jsp").forward(request, response);
			}else {
				request.getRequestDispatcher("pair/fail.jsp").forward(request, response);
			}
		}else if(a.equals("admin")){
			String name = request.getParameter("username");
			String pwd = request.getParameter("userpwd");
			User u=new User();
			User u2=new User();
			u.setUsername(name);
			u.setUserpwd(pwd);
			UserDao ud=new UserDao();
			u2=ud.findAdmin(u);
			List<Task> l=ud.findAll();
			if(u2!=null) {
				session.setAttribute("username", name);
				request.setAttribute("list", l);
				request.getRequestDispatcher("vote/success.jsp").forward(request, response);
			}else {
				request.getRequestDispatcher("pair/fail.jsp").forward(request, response);
			}
		}
		
//		String name = request.getParameter("username");
//		String pwd = request.getParameter("userpwd");
//		User u=new User();
//		User u2=new User();
//		u.setUsername(name);
//		u.setUserpwd(pwd);
//		UserDao ud=new UserDao();
//		u2=ud.findUser(u);
//		List l=ud.findAll();
//		if(u2!=null) {
//			request.setAttribute("username", name);
//			request.setAttribute("list", l);
//			request.getRequestDispatcher("pair/success.jsp").forward(request, response);
//		}else {
//			request.getRequestDispatcher("pair/fail.jsp").forward(request, response);
//		}
			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
	}

}
