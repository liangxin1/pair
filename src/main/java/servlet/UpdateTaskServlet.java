package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Task;
import dao.JdbcUtil;
import dao.TaskDao;
import dao.UserDao;

/**
 * Servlet implementation class UpdateTaskServlet
 */
public class UpdateTaskServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateTaskServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String owner=(String) request.getParameter("owner");
		String status=(String) request.getParameter("status");
		String content=(String) request.getParameter("content");
		String start=(String) request.getParameter("start");
		String end=(String) request.getParameter("end");
		String id=(String) request.getParameter("id");
		SimpleDateFormat sf=new SimpleDateFormat("yyyy-MM-dd");
		String rs="N";
		TaskDao udao=new TaskDao();
		Task t=new Task();
		t.setName(owner);
		t.setcontent(content);
		t.setState(status);
			try {
				t.setBtime(new java.sql.Date(sf.parse(start).getTime()));
				t.setEtime(new java.sql.Date(sf.parse(end).getTime()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
		//String 转 int
		int aid=Integer.parseInt(id);
		t.setId(aid);
		if(udao.updateTask(t)){
			rs="Y";
		}
		response.getWriter().append(rs);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
